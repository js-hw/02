let userName = prompt("What is your name?", "");

while (true) {
  // Універсальний метод для обох варіантів "Ok" та "Esc":
  // if (!userName) {
  // userName = prompt("You've entered either nothin or escape. Enter your name","");
  // }

  if (userName === null) {
    userName = prompt("You can't escape the answer! Enter your name", "");
  } else if (userName === "") {
    userName = prompt("You've entered nothing. Enter your name", "");
  } else if (userName.trim() === "") {
    userName = prompt("You've entered only spaces. Need your name", "");
  } else {
    break;
  }
}

let userAge = prompt("How old are you?", "");

while (true) {
  if (userAge == null) {
    userAge = prompt("You can't escape the answer! Enter your age", "");
  } else if (!userAge) {
    userAge = prompt("You've entered nothing. Enter your age", `${userAge}`);
  } else if (isNaN(parseInt(userAge)) || isNaN(userAge)) {
    userAge = prompt("Please, enter your correct age", `${userAge}`);
  } else if (userAge > 100) {
    userAge = prompt("It's too much!", `${userAge}`);
  } else if (userAge >= 0 && userAge < 3) {
    userAge = prompt("Too few! You must be more 2 years old", `${userAge}`);
  } else if (userAge < 0) {
    userAge = prompt(
      "Too much negativity. Need more positivity for age",
      `${userAge}`
    );
  } else {
    break;
  }
}

if (userAge > 22) {
  alert(`Welcome, ${userName}`);
} else if (userAge >= 18 && userAge <= 22) {
  userAge = confirm("Are you sure you want to continue?");
  if (userAge === true) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert("You are not allowed to visit this website");
}
